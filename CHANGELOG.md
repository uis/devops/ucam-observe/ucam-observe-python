# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0]
## Changed
- `gunicorn.conf.py` now requires just one import (and it actually works)

## [0.1.2] - 2024-05-15
### Changed
 - Refactor logging configuration and format

## [0.1.1] - 2024-05-15
### Changed
 - Update README.md

## [0.1.0] - 2024-05-03
### Changed
 - Add get_structlog_logger and update logging configurations

## [0.0.1] - 2024-04-22
### Added
 - Initial version.
