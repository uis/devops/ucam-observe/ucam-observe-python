import pytest


@pytest.mark.no_django  # Will not run with default pytest command (use/see tox)
def test_django_init_without_django_structlog_installed():
    with pytest.raises(ImportError) as e:
        from ucam_observe.django import hello_world_django

        hello_world_django()  # Should never run

    assert str(e.value) == "ucam-observe[django] is required to use this module", "This test "


def test_hello_world_django_with_django_structlog_installed():
    from ucam_observe.django import hello_world_django

    assert (
        hello_world_django() == "hello world Django"
    ), "The method should return the correct message"
