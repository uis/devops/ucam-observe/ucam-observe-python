import logging
import os
from unittest.mock import patch

import pytest
import structlog

from ucam_observe.logging_config import (
    configure_logging,
    get_console_logging_status,
    get_log_level_name,
)


@pytest.fixture
def set_log_level_debug():
    with patch.dict(os.environ, {"LOG_LEVEL": "DEBUG"}):
        yield


@pytest.fixture
def set_console_logging_status():
    with patch.dict(os.environ, {"CONSOLE_LOGGING": "True"}):
        yield


def test_get_log_level(set_log_level_debug):
    log_level = get_log_level_name()
    assert log_level == "DEBUG"


def test_console_logging_status(set_console_logging_status):
    console_logging_status = get_console_logging_status()
    assert console_logging_status is True


def test_configure_logging(set_log_level_debug):
    configure_logging()

    logger = logging.getLogger()

    assert logger.level == logging.DEBUG
    assert len(logger.handlers) == 1
    assert isinstance(logger.handlers[0], logging.StreamHandler)
    assert isinstance(logger.handlers[0].formatter, structlog.stdlib.ProcessorFormatter)
