import pytest


# Pattern from https://docs.pytest.org/en/latest/example/simple.html#control-skipping-of-tests-according-to-command-line-option # noqa E501
def pytest_addoption(parser):
    parser.addoption(
        "--no-django",
        action="store_true",
        default=False,
        help="Run tests that must not have have django optional dependencies installed.",
    )


def pytest_configure(config):
    config.addinivalue_line(
        "markers",
        "no_django: tests that must not have have django optional dependencies installed",
    )


def pytest_collection_modifyitems(config, items):
    if config.getoption("--no-django"):
        skip = pytest.mark.skip(reason="can't run with --no-django option")
        for item in items:
            if "no_django" not in item.keywords:
                item.add_marker(skip)
        return
    skip = pytest.mark.skip(reason="need --no-django option to run")
    for item in items:
        if "no_django" in item.keywords:
            item.add_marker(skip)
