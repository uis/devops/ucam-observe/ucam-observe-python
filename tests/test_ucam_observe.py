import pytest

from ucam_observe import configure_logging, get_structlog_logger


@pytest.fixture(autouse=True)
def setup_logging(caplog):
    configure_logging()
    caplog.set_level("INFO")


def test_logger_level(caplog):
    logger = get_structlog_logger("test_logger1")
    logger.debug("debug message")
    logger.info("info message")

    assert "debug message" not in caplog.text, "Debug message should not be logged at INFO level"
    assert "info message" in caplog.text, "Info message should be logged at INFO level"


def test_structured_logger(caplog):
    logger = get_structlog_logger("test_logger2")
    logger.info("test message", key="value")

    assert "test message" in caplog.text
    assert "key" in caplog.text
    assert "value" in caplog.text
