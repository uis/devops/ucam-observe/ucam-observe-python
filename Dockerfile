FROM python:3.9-slim AS base
LABEL maintainer="University of Cambridge Information Services DevOps Division <devops@uis.cam.ac.uk>"

WORKDIR /usr/src/app

# Pre-install requirements to make re-build times smaller when developing.
RUN pip install --no-cache-dir poetry

# Copy only the relevant files required to run the project
COPY pyproject.toml poetry.lock* /usr/src/app/

# Install runtime dependencies using Poetry
RUN poetry config virtualenvs.create false \
    && poetry install --no-interaction --no-ansi --all-extras

# Copy only the relevant files required to run the project
COPY ./ucam_observe/ /usr/src/app/
